package ru.t1.sochilenkov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    public static final String APPLICATION_NAME_DEFAULT = "tm";

    @NotNull
    public static final String APPLICATION_LOG_KEY = "application.log";

    @NotNull
    public static final String APPLICATION_LOG_DEFAULT = "./";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "3568548474";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "436266236";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "500";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String DB_URL_DEFAULT = "jdbc:postgresql://localhost:5432/postgres?currentSchema";

    @NotNull
    private static final String DB_URL_KEY = "database.url";

    @NotNull
    private static final String DB_PASSWORD_DEFAULT = "21356";

    @NotNull
    private static final String DB_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DB_USER_DEFAULT = "postgres";

    @NotNull
    private static final String DB_USER_KEY = "database.username";

    @NotNull
    private static final String DB_SCHEMA_DEFAULT = "tm";

    @NotNull
    private static final String DB_SCHEMA_KEY = "database.schema";

    @NotNull
    private static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DB_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DB_CACHE_DEFAULT = "false";

    @NotNull
    private static final String DB_CACHE_KEY = "database.l2Cache";

    @NotNull
    private static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQL95Dialect";

    @NotNull
    private static final String DB_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DB_SHOW_SQL_DEFAULT = "false";

    @NotNull
    private static final String DB_SHOW_SQL_KEY = "database.showSQL";

    @NotNull
    private static final String DB_HBM2DDL_DEFAULT = "none";

    @NotNull
    private static final String DB_HBM2DDL_KEY = "database.hbm2ddl";

    @NotNull
    private static final String DB_CACHE_REGION_KEY = "database.cacheRegionClass";

    @NotNull
    private static final String DB_CACHE_REGION_DEFAULT = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";

    @NotNull
    private static final String DB_QUERY_CACHE_KEY = "database.useQueryCache";

    @NotNull
    private static final String DB_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DB_MINIMAL_PUTS_KEY = "database.useMinPuts";

    @NotNull
    private static final String DB_MINIMAL_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DB_CACHE_REGION_PREFIX_KEY = "database.regionPrefix";

    @NotNull
    private static final String DB_CACHE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String DB_CACHE_PROVIDER_KEY = "database.configFilePath";

    @NotNull
    private static final String DB_CACHE_PROVIDER_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String JMS_BROKER_KEY = "jms.brokerUrl";

    @NotNull
    private static final String JMS_BROKER_DEFAULT = "tcp://localhost:61616";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    @Override
    public String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationLog() {
        return getStringValue(APPLICATION_LOG_KEY, APPLICATION_LOG_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return Integer.parseInt(getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return Integer.parseInt(getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT));
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY, SESSION_KEY_DEFAULT);
    }

    @Override
    public Integer getSessionTimeout() {
        return Integer.parseInt(getStringValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT));
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DB_URL_KEY, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DB_PASSWORD_KEY, DB_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DB_USER_KEY, DB_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBSchema() {
        return getStringValue(DB_SCHEMA_KEY, DB_SCHEMA_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DB_DRIVER_KEY, DB_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBL2Cache() {
        return getStringValue(DB_CACHE_KEY, DB_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DB_DIALECT_KEY, DB_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSQL() {
        return getStringValue(DB_SHOW_SQL_KEY, DB_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHbm2DDL() {
        return getStringValue(DB_HBM2DDL_KEY, DB_HBM2DDL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheRegion() {
        return getStringValue(DB_CACHE_REGION_KEY, DB_CACHE_REGION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBQueryCache() {
        return getStringValue(DB_QUERY_CACHE_KEY, DB_QUERY_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBMinimalPuts() {
        return getStringValue(DB_MINIMAL_PUTS_KEY, DB_MINIMAL_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheRegionPrefix() {
        return getStringValue(DB_CACHE_REGION_PREFIX_KEY, DB_CACHE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheProvider() {
        return getStringValue(DB_CACHE_PROVIDER_KEY, DB_CACHE_PROVIDER_DEFAULT);
    }

    @NotNull
    @Override
    public String getJMSBrokerURL() {
        return getStringValue(JMS_BROKER_KEY, JMS_BROKER_DEFAULT);
    }

}
