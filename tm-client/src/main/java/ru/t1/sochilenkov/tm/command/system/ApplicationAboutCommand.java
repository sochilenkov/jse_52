package ru.t1.sochilenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Display author info.";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + service.getApplicationName());
        System.out.println();

        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + service.getAuthorName());
        System.out.println("E-MAIL: " + service.getAuthorEmail());
        System.out.println();
    }

}
