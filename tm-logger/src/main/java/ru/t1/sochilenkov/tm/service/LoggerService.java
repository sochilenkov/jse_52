package ru.t1.sochilenkov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.ILoggerService;

import org.bson.Document;
import java.util.LinkedHashMap;
import java.util.Map;


@NoArgsConstructor
public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient = new MongoClient(getDBHost(), getDBPort());

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase(getDBName());


    @Override
    @SneakyThrows
    public void writeLog(@NotNull final String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();

        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }

    private String getDBHost() {
        if (System.getenv().containsKey("MONGO_HOST")) return System.getenv("MONGO_HOST");
        return "192.168.1.72";
    }

    private int getDBPort() {
        if (System.getenv().containsKey("MONGO_PORT")) return Integer.valueOf(System.getenv("MONGO_PORT"));
        return 27017;
    }

    private String getDBName() {
        if (System.getenv().containsKey("MONGO_DB_NAME")) return System.getenv("MONGO_DB_NAME");
        return "tm-log";
    }

}
